function verificaQuatidadeDeCaracterEseTemNumero(str){
    let cont = 0;
    for (let caracter of str) {
        if(caracter != " ") {
            cont++
        }
                                //Esta lógica tira o caso de erro de  " " ser um número
        if((! isNaN(caracter)) && (caracter != " ")) {
            return [false];
        }
    }

    return [true, cont];
}

// valida se não contém número e se está dentro do intervalo de caracteres
function validaNome(nome, intervalo1, intervalo2){
    const resposta = verificaQuatidadeDeCaracterEseTemNumero(nome);

     if((resposta[0]) && (resposta[1] > intervalo1) && (resposta[1] < intervalo2)) {
         return true;
     }

     return false;
}

function validaIdade(idade){
    idade = Number(idade);

    if((! isNaN(idade)) && (Number(idade) > 0) && Number(idade) < 100){
        return true;
    }

    return false;
}
function checkImagem(url) {
    var img = '<img src="'+ url +'" />';
    $(img).load(function() {
      $('body').append(url+img);
    }).bind('error', function() {
      alert('imagem: '+url+' não existe');
    });
   }


function cadastro() {
    const name = document.querySelector(".texto1").value;
    const age = document.querySelector(".texto2").value;
    const link = document.querySelector(".texto3").value;
    const description = document.querySelector(".texto4").value;

    document.querySelector(".texto1").value = "";
    document.querySelector(".texto2").value = "";
    document.querySelector(".texto3").value = "";
    document.querySelector(".texto4").value = "";

    if((! validaNome(name, 4, 60))){
        return [false];
    }

    if((! validaIdade(age))){
        return [false];
    }

    if((! validaNome(description, 10, 255))){
        return [false];
    }

    //Falta colocar uma function para verifivar se a url da imagem existe.

    return [true, {
        wolf: {
        name: name,
        age: age,
        image_url: link,
        description: description
        }
    }]
}

const btn = document.querySelector(".btn-salvar")
btn.addEventListener("click", () => {
    const url = "http://lobinhos.herokuapp.com/wolves"
    const resposta = cadastro()

    if((! resposta[0])) {
        console.log("Erro no cadastro!")
        return 0;
    }

    const fetchConfig = {
        method: "POST",
        body: JSON.stringify(resposta[1]),
        headers: {"Content-Type":"application/json"}
    }

    fetch(url, fetchConfig).then(response => {
        response.json().then(() => {
        }).catch(err => {
            console.log(err);
        })
    }).catch(err => {
        console.log(err);
    })
})
