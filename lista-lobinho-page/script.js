//Busca na Api lobos adotados
fetch("http://lobinhos.herokuapp.com/wolves/adopted").then(response => {
    response.json().then(lobosAdotados => {
        MostraNaTela(lobosAdotados);
    }).catch(erro => {
        console.log(erro);
    })
}).catch(erro => {
    console.log(erro);
})

//Busca na Api lobos disponíveis para adoção
fetch("http://lobinhos.herokuapp.com/wolves").then(response => {
    response.json().then(lobosAdocao => {
        MostraNaTela(lobosAdocao);
    }).catch(erro => {
        console.log(erro);
    })
}).catch(erro => {
    console.log(erro);
})

//função que verifica se o lobo pode ser adotado ou não
function configBotao(objeto) {
    if(objeto.adopted) {
        return ["Adotado","bnt0"]
    }
    return ["Adotar", "bnt1"]
}

//Mostra Lobinhos Adotados
// function MostraAdotados(){

// }



function MostraNaTela(array){
    const conteiner = document.querySelector(".container");


    for (let index in array){
        const div = document.createElement("div");
        div.classList.add("guarda-mensagem");
        const botao = configBotao(array[index]);

        if(index%2 === 0) {
            div.innerHTML = `
            <div class="lobo-corpo">
                <div class="lobo-imagem">
                    <img src="${array[index].image_url}" alt="lobo" class="img-api1">
                    <div class="imagem-azul1"></div>
                </div>

                <div class="lobo-texto">
                    <input type="button" value="${botao[0]}" id="${botao[1]}" class="btn1">
                    <div class="lobo-nome1">${array[index].name}</div>
                    <div class="lobo-idade1">Idade: ${array[index].age}</div>
                    <div class="lobo-descricao1">${array[index].description}</div>
                </div>
            </div>
            `
        }else {
            div.innerHTML = `
            <div class="lobo-corpo">

                <div class="lobo-texto">

                    <input type="button" value="${botao[0]}" id="${botao[1]}" class="btn2">
                    <div class="lobo-nome2">${array[index].name}</div>
                    <div class="lobo-idade2">Idade: ${array[index].age}</div>
                    <div class="lobo-descricao2">${array[index].description}</div>
                </div>

                <div class="lobo-imagem">
                    <img src="${array[index].image_url}" alt="lobo" class="img-api2">
                    <div class="imagem-azul2"></div>
                </div>

            </div>`
        }

        conteiner.appendChild(div);
    }   
}




const checkbox = document.querySelector("checkbox");

checkbox.click(function(){
    if( $(this).is(":checked") ){
        alert("foi checado");
    }else{
        alert("foi deschecado");
    }
 });
 
 checkbox.trigger("click");